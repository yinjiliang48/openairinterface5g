#/*
# * Licensed to the OpenAirInterface (OAI) Software Alliance under one or more
# * contributor license agreements.  See the NOTICE file distributed with
# * this work for additional information regarding copyright ownership.
# * The OpenAirInterface Software Alliance licenses this file to You under
# * the OAI Public License, Version 1.1  (the "License"); you may not use this file
# * except in compliance with the License.
# * You may obtain a copy of the License at
# *
# *      http://www.openairinterface.org/?page_id=698
# *
# * Unless required by applicable law or agreed to in writing, software
# * distributed under the License is distributed on an "AS IS" BASIS,
# * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.# * See the License for the specific language governing permissions and
# * limitations under the License.
# *-------------------------------------------------------------------------------
# * For more information about the OpenAirInterface (OAI) Software Alliance:
# *      contact@openairinterface.org
# */
#---------------------------------------------------------------------
#
# Dockerfile for the Open-Air-Interface BUILD service
#   Valid for Ubuntu 18.04
#
#---------------------------------------------------------------------


FROM ubuntu:bionic AS ran-base
ARG NEEDED_GIT_PROXY
ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Paris
ENV BUILD_UHD_FROM_SOURCE=True
ENV UHD_VERSION=3.15.0.0

## to protect against sporadic fail to download with apt-get
#   this source recommends swapping out archive.ubuntu.com with
#   us.archive.ubuntu.com
# https://stackoverflow.com/questions/37706635/in-docker-apt-get-install-fails-with-failed-to-fetch-http-archive-ubuntu-com
RUN sed -i'' 's/archive\.ubuntu\.com/us\.archive\.ubuntu\.com/' /etc/apt/sources.list


#install developers pkg/repo
RUN apt-get update  && DEBIAN_FRONTEND=noninteractive apt-get upgrade --yes && DEBIAN_FRONTEND=noninteractive apt-get install --yes -f build-essential \
psmisc \
git \
xxd \
unzip \
python3-pip \
graphviz \
xkb-data \
libncurses5-dev \
libncursesw5-dev \
apt-utils \
apt-transport-https \
cmake \
libforms2 \
sudo \
subversion \
mono-devel \  
lmodern \
libgnutls28-dev \
xkb-data \
openjdk-11-jdk \
libboost-all-dev \
libc6-dev \
libatlas-base-dev \
liblapack-dev \
libopenblas-base \
flex \
debhelper \
libxaw7-dev \
libfontconfig \
multiarch-support \ 
libc6 \
libgcc1 

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get upgrade --yes && DEBIAN_FRONTEND=noninteractive apt-get install -y -f libpixman-1-dev \
libpixman-1-0 \
libglx-mesa0 \
 mesa-common-dev

RUN pip3 install --ignore-installed pyyaml


# In some network environments, GIT proxy is required
#RUN /bin/bash -c "if [[ -v NEEDED_GIT_PROXY ]]; then git config --global http.proxy $NEEDED_GIT_PROXY; fi"

#create the WORKDIR
WORKDIR /oai-ran
COPY . .



#run build_oai -I to get the builder image
RUN /bin/sh oaienv &&\
cd cmake_targets &&\
mkdir -p log && \ 
./build_oai -I -w USRP
