#!/bin/sh

#installation of lapack library
curl -o lapack-3.7.1.tgz http://www.netlib.org/lapack/lapack-3.7.1.tgz
tar -zxvf lapack-3.7.1.tgz
cd lapack-3.7.1
mkdir build
cd build
# may need to install gfortran
#apt-get update && apt-get install -y -f gfortran

cmake -DCMAKE_INSTALL_LIBDIR=$HOME/.local/lapack ..
# this will take a WHILE
cmake --build . --target install
cd .. 
cd ..
#cleanup 
rm -rf lapack-3.7.1.tgz lapack-3.7.1


