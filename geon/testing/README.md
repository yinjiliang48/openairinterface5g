#  OAI for ARM Testing
## Test Execution 

In order to run oai in rfsim mode on a intel based ubuntu based system for comparision pourposes run the following commands: 
```
docker compose -f oai_gnb_ue_rfsim.yml up 
``` 

In order to run oai in rfsim mode on an arm based ubuntu system run the following commands: 
```
docker compose -f oai_gnb_ue_rfsim_arm.yml up 
```

In order to run oai UE using a B-series USRP  on an arm bases  
```
docker compose -f ue_only_sdr_arm.yml up 
```

## Curent Status

On the RB5 drone platform that we used for testing the current status.  OAI in RFSIM mode has been tested to work properly.    

When the OAI UE executable was tested on the platform the following error messages appear: 
```
sdr-rfsim-oai-nr-ue  | Assertion (UE->frame_parms.get_samples_per_slot(slot,&UE->frame_parms) == UE->rfdevice.trx_read_func(&UE->rfdevice, timestamp, rxp, UE->frame_parms.get_samples_per_slot(slot,&UE->frame_parms), UE->frame_parms.nb_antennas_rx)) failed!
sdr-rfsim-oai-nr-ue  | In readFrame() /oai-ran/executables/nr-ue.c:765
sdr-rfsim-oai-nr-ue  | 
sdr-rfsim-oai-nr-ue  | Exiting execution
```

The expected reason that it is failing is because there is an I/O limitation of this platform for streaming I/Q from the SDR.  Ettus provides a benchmarking application with their UHD instaltion and this caps out at 15MSps Tx/Rx.  

It is possible the if we pair down the execution profile of the OAI to say SSB decoding that we may be able to create a funtional executable on this platform.  